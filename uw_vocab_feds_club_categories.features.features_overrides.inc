<?php

/**
 * @file
 * uw_vocab_feds_club_categories.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_vocab_feds_club_categories_features_override_default_overrides() {
  // This code is only used for UI in features. Export alter hooks do the magic.
  $overrides = array();

  // Exported overrides for: node.
  $overrides["node.feds_club.description"] = 'Feds club listing details and contact information.';
  $overrides["node.feds_club.name"] = 'Feds club';

  // Exported overrides for: taxonomy.
  $overrides["taxonomy.feds_club_categories.description"] = 'Which categories apply to the club?';
  $overrides["taxonomy.feds_club_categories.name"] = 'Feds club categories';

  // Exported overrides for: variable.
  $overrides["variable.pathauto_node_feds_club_pattern.value"] = '/clubs/[node:title]';

  return $overrides;
}
